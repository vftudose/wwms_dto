<?php

namespace Craft\Listener;

use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Craft\Event\PublishableEvent;

class RabbitMqPublisher
{
    private  $producer;

    public function __construct(ProducerInterface $producer)
    {
        $this->producer = $producer;
    }

    public function onPublish(PublishableEvent $event)
    {
        $this->producer->publish(json_encode([
            'name' => $event->getName(),
            'data' => $event->getData()
        ]));
    }
}