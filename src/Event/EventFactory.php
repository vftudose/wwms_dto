<?php

namespace Craft\Event;

use Craft\Dto\Order\Order;
use Craft\Dto\Reception;
use Craft\Dto\Task;
use Symfony\Contracts\EventDispatcher\Event;

final class EventFactory
{
    const ORDER_CREATED_EVENT = OrderCreatedEvent::NAME;
    const ORDER_RESERVED_EVENT = OrderReservedEvent::NAME;
    const ORDER_UNRESERVED_EVENT = OrderUnreservedEvent::NAME;
    const ORDER_TASKS_GENERATED_EVENT = OrderTasksGeneratedEvent::NAME;

    const TASK_PICKED_EVENT = TaskPickedEvent::NAME;

    const RECEPTION_CREATED_EVENT = ReceptionCreatedEvent::NAME;
    const RECEPTION_TASKS_GENERATED_EVENT = ReceptionTasksGeneratedEvent::NAME;


    const ALLOWED_EVENTS = [
        self::ORDER_CREATED_EVENT,
        self::ORDER_RESERVED_EVENT,
        self::ORDER_UNRESERVED_EVENT,
        self::ORDER_TASKS_GENERATED_EVENT,
        self::RECEPTION_CREATED_EVENT,
        self::RECEPTION_TASKS_GENERATED_EVENT,
        self::TASK_PICKED_EVENT
    ];

    public static function get(string $name, array $data):? Event
    {
        if (!in_array($name, self::ALLOWED_EVENTS)) {
            return null;
        }
        
        switch ($name) {
            case self::ORDER_CREATED_EVENT:
                return new OrderCreatedEvent(new Order($data));
            case self::ORDER_UNRESERVED_EVENT:
                return new OrderUnreservedEvent(new Order($data));
            case self::ORDER_RESERVED_EVENT:
                return new OrderReservedEvent(new Order($data));
            case self::ORDER_TASKS_GENERATED_EVENT:
                return new OrderTasksGeneratedEvent(new Order($data));
            case self::RECEPTION_CREATED_EVENT:
                return new ReceptionCreatedEvent(new Reception($data));
            case self::RECEPTION_TASKS_GENERATED_EVENT:
                return new ReceptionTasksGeneratedEvent(new Reception($data));
            case self::TASK_PICKED_EVENT:
                return new TaskPickedEvent(new Task($data));
            default:
                return null;
        }
    }
}