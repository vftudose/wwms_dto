<?php


namespace Craft\Event;


use Craft\Dto\Reception as ReceptionDto;

interface ReceptionEventInterface
{
    public function getReception(): ReceptionDto;
}