<?php

namespace Craft\Event;

use Craft\Dto\Order\Order as OrderDto;
use Symfony\Contracts\EventDispatcher\Event;

 final class OrderReservedEvent extends Event implements PublishableEvent, OrderEventInterface
{
    const NAME = 'order_reserved_event';

    private $order;

    public function __construct(OrderDto $order)
    {
        $this->order = $order;
    }

    public function getData(): array
    {
        return $this->order->toArray();
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getOrder(): OrderDto
    {
        return $this->order;
    }
}