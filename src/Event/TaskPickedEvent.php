<?php

namespace Craft\Event;

use Craft\Dto\Task;
use Symfony\Contracts\EventDispatcher\Event;

class TaskPickedEvent extends Event implements PublishableEvent
{
    const NAME = 'task_picked_event';

    private $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getData(): array
    {
        return $this->task->toArray();
    }
}