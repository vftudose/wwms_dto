<?php

namespace Craft\Event;

use Craft\Dto\Reception as ReceptionDto;
use Symfony\Contracts\EventDispatcher\Event;

final class ReceptionCreatedEvent extends Event implements PublishableEvent, ReceptionEventInterface
{
    const NAME = 'reception_created_event';

    private $reception;

    public function __construct(ReceptionDto $reception)
    {
        $this->reception = $reception;
    }

    public function getData(): array
    {
        return $this->reception->toArray();
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getReception(): ReceptionDto
    {
        return $this->reception;
    }
}