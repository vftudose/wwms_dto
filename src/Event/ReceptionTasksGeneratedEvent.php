<?php

namespace Craft\Event;

use Craft\Dto\Reception as ReceptionDto;
use Symfony\Contracts\EventDispatcher\Event;

class ReceptionTasksGeneratedEvent extends Event implements ReceptionEventInterface, PublishableEvent
{
    const NAME = 'reception_tasks_generated_event';

    private $reception;

    public function __construct(ReceptionDto $reception)
    {
        $this->reception = $reception;
    }

    public function getData(): array
    {
        return $this->reception->toArray();
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getReception(): ReceptionDto
    {
        return $this->reception;
    }
}