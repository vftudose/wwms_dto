<?php


namespace Craft\Event;


use Craft\Dto\Order\Order as OrderDto;

interface OrderEventInterface
{
    public function getOrder() : OrderDto;
}