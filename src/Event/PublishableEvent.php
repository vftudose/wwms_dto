<?php

namespace Craft\Event;

interface PublishableEvent
{
    const EVENT_NAME = 'publishable_event';

    public function getName(): string;
    public function getData(): array;
}