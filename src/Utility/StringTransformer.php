<?php

namespace Craft\Utility;

class StringTransformer
{
    public static function toCamelCase($input, $separator = '-')
    {
        $words = explode($separator, $input);

        if (count($words) == 1) {
            return $words[0];
        }

        $parts = array_map('ucwords', $words);

        return lcfirst(implode('', $parts));
    }

    public static function toSnakeCase(string $input, string $separator = '-'): string
    {
        if (preg_match('/[A-Z]/', $input) === 0) {
            return $input;
        }

        $pattern = '/([a-z])([A-Z])/';

        return strtolower(preg_replace_callback($pattern, function ($a) use ($separator) {
            return $a[1] . $separator . strtolower($a[2]);
        }, $input));
    }
}