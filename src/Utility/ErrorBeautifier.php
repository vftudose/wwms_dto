<?php

namespace Craft\Utility;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ErrorBeautifier
{
    public static function beautifyAsJson(ConstraintViolationListInterface $errors): string
    {
        return json_encode(self::beautifyAsArray($errors));
    }

    public static function beautifyAsArray(ConstraintViolationListInterface $errors): array
    {
        $messages = [];
        foreach ($errors as $error) {
            $messages[$error->getPropertyPath()] = $error->getMessage();
        }

        return $messages;
    }
}