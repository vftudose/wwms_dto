<?php

namespace Craft\Consumer;

use Exception;
use Craft\Event\EventFactory;
use PhpAmqpLib\Message\AMQPMessage;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class RabbitMqConsumer
{
    private $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $message = \json_decode($msg->getBody(), true);

        $event = EventFactory::get($message['name'], $message['data']);

        if ($event == null) {
            return true;
        }

        try {
            $this->dispatcher->dispatch($event, $message['name']);
        } catch (Exception $exception) {
            return false;
        }

        return true;
    }
}


