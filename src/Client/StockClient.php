<?php

namespace Craft\Client;

use Craft\Dto\Zone as ZoneDto;
use Craft\Dto\Shelf as ShelfDto;
use Craft\Dto\Location as LocationDto;
use Craft\Dto\Container as ContainerDto;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class StockClient
{
    const ZONES_PATH = '/zones';
    const SHELVES_PATH = '/shelves';
    const LOCATIONS_PATH = '/locations';
    const CONTAINERS_PATH = '/containers';
    const AVAILABLE_LOCATIONS_PATH = '/available-locations';

    private $guzzle;
    private $host;

    public function __construct(string $host)
    {
        $this->guzzle = new Client([
            'defaults' => [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80',
                    'Content-Type' => 'application/json'
                ]
            ]
        ]);

        $this->host = $host;
    }

    public function createZone(ZoneDto $zone): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::ZONES_PATH);

        try {
            return $this->guzzle->post($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
                RequestOptions::JSON => $zone->toArray()
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getZone(int $zoneId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::ZONES_PATH);

        try {
            return $this->guzzle->get($uri . '/' . $zoneId, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getZoneByName(string $name): ResponseInterface
    {
        return $this->getAllZones(1, $name);
    }

    public function getAllZones(int $page, string $name = null): ResponseInterface
    {
        $query = 'page=' . $page;

        if ($name) {
            $query .= '&name=' . $name;
        }

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::ZONES_PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function deleteZone(int $zoneId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::ZONES_PATH);

        try {
            return $this->guzzle->delete($uri . '/' . $zoneId, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function createShelf(ShelfDto $shelf): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::SHELVES_PATH);

        try {
            return $this->guzzle->post($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
                RequestOptions::JSON => $shelf->toArray()
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getShelf(int $shelfId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::SHELVES_PATH);

        try {
            return $this->guzzle->get($uri . '/' . $shelfId, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getAllShelves(int $page): ResponseInterface
    {
        $query = 'page=' . $page;

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::SHELVES_PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function deleteShelf(int $shelfId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::SHELVES_PATH);

        try {
            return $this->guzzle->delete($uri . '/' . $shelfId, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function createLocation(LocationDto $location): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::LOCATIONS_PATH);

        try {
            return $this->guzzle->post($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
                RequestOptions::JSON => $location->toArray()
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getLocation(int $locationId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::LOCATIONS_PATH);

        try {
            return $this->guzzle->get($uri . '/' . $locationId, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getAllLocations(int $page = 1, string $barcode = null): ResponseInterface
    {
        $query = 'page=' . $page;

        if ($barcode) {
            $query .= '&barcode=' . $barcode;
        }

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::LOCATIONS_PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function deleteLocation(int $locationId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::LOCATIONS_PATH);

        try {
            return $this->guzzle->delete($uri . '/' . $locationId, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function createContainer(ContainerDto $container): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::CONTAINERS_PATH);

        try {
            return $this->guzzle->post($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
                RequestOptions::JSON => $container->toArray()
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getContainer(int $containerId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::CONTAINERS_PATH);

        try {
            return $this->guzzle->get($uri . '/' . $containerId, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getAllContainers(int $page, string $barcode = null): ResponseInterface
    {
        $query = 'page=' . $page;

        if ($barcode) {
            $query .= '&barcode=' . $barcode;
        }

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::CONTAINERS_PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function deleteContainer(int $containerId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::CONTAINERS_PATH);

        try {
            return $this->guzzle->delete($uri . '/' . $containerId, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getAvailableLocations(array $productIds): ResponseInterface
    {
        $query = sprintf('products=%s', implode(",", $productIds));

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::AVAILABLE_LOCATIONS_PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getContainerStockLines(int $containerId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::CONTAINERS_PATH . '/' . $containerId . '/' . 'stock');

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }
}
