<?php

namespace Craft\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Craft\Dto\Product as ProductDto;

class ProductsClient
{
    const PATH = '/products';

    private $guzzle;
    private $host;

    public function __construct(string $host)
    {
        $this->guzzle = new Client([
            'defaults' => [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80',
                    'Content-Type' => 'application/json'
                ]
            ]
        ]);

        $this->host = $host;
    }

    public function create(ProductDto $productDto): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::PATH);

        try {
            return $this->guzzle->post($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
                RequestOptions::JSON => $productDto->toArray()
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function update(int $id, ProductDto $productDto): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::PATH . '/' . $id);

        try {
            return $this->guzzle->put($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
                RequestOptions::JSON => $productDto->toArray()
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function show(int $id): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::PATH . '/' . $id);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function delete(int $id): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::PATH . '/' . $id);

        try {
            return $this->guzzle->delete($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getProductsByIds(array $ids, int $page = 1): ResponseInterface
    {
        $query = 'page=' . $page;

        foreach ($ids as $id) {
            $query .= '&ids[]=' . $id;
        }

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getProductByBarcode(string $barcode)
    {
        $query = 'barcode=' . $barcode;

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }
}