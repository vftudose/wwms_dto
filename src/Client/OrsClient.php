<?php

namespace Craft\Client;

use Craft\Dto\Order\Order as OrderDto;
use Craft\Dto\Reception as ReceptionDto;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class OrsClient
{
    const ORDER_PATH = '/orders';
    const RECEPTION_PATH = '/receptions';

    private $guzzle;
    private $host;

    public function __construct(string $host)
    {
        $this->guzzle = new Client([
            'defaults' => [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80',
                    'Content-Type' => 'application/json'
                ]
            ]
        ]);

        $this->host = $host;
    }

    public function createOrder(OrderDto $orderDto): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::ORDER_PATH);

        try {
            return $this->guzzle->post($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
                RequestOptions::JSON => $orderDto->toArray()
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getOrder($id): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::ORDER_PATH . '/' . $id);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ]
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getAllOrders($page = 1): ResponseInterface
    {
        $query = 'page=' . $page;

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::ORDER_PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ]
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function createReception(ReceptionDto $receptionDto): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::RECEPTION_PATH);

        try {
            return $this->guzzle->post($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
                RequestOptions::JSON => $receptionDto->toArray()
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getReception($receptionId): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::RECEPTION_PATH . '/' . $receptionId);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function getAllReceptions($page = 1): ResponseInterface
    {
        $query = 'page=' . $page;

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::RECEPTION_PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ]
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }
}