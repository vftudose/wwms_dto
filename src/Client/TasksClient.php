<?php

namespace Craft\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class TasksClient
{

    const LOGIN_PATH = '/pda/login';
    const OPERATIONS_PATH = '/pda/operations';
    const PICKING_PATH = '/pda/operations/picking';
    const REPLENISHMENT_PATH = '/pda/operations/replenishment';
    const ORDER_PICKING_PATH = '/pda/operations/picking/order-picking/scan-location';

    private $guzzle;
    private $host;

    public function __construct(string $host)
    {
        $this->guzzle = new Client([
            'defaults' => [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80',
                    'Content-Type' => 'application/json'
                ]
            ]
        ]);

        $this->host = $host;
    }

    public function login(array $params): ResponseInterface
    {
        $query = 'error=' . $params['error'];

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::LOGIN_PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function operations(): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::OPERATIONS_PATH);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ]
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function picking(): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::PICKING_PATH);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ]
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function replenishment(): ResponseInterface
    {
        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::REPLENISHMENT_PATH);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ]
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }

    public function orderPicking(string $userId): ResponseInterface
    {
        $query = 'userId=' . $userId;

        $uri = (new Uri())
            ->withHost($this->host)
            ->withPath(self::ORDER_PICKING_PATH)
            ->withQuery($query);

        try {
            return $this->guzzle->get($uri, [
                RequestOptions::HEADERS => [
                    'Host' => 'localhost:80'
                ]
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }
    }
}
