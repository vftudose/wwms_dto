<?php

namespace Craft\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class ReceptionLine extends DataTransferObject
{
    public $id;

    /**
     * @Assert\Type("integer")
     * @Assert\NotBlank()
     */
    public $productId;

    /**
     * @Assert\Type("integer")
     * @Assert\NotBlank()
     */
    public $expectedQuantity;

    /**
     * @Assert\Type("integer")
     */
    public $receivedQuantity;

}
