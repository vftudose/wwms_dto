<?php

namespace Craft\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class Zone extends DataTransferObject
{
    const TYPE_STORAGE = 'storage';
    const TYPE_PICKING = 'picking';
    const TYPE_PACKAGE = 'package';
    const TYPE_EXIT = 'exit';

    const ALLOWED_TYPES = [
        self::TYPE_STORAGE,
        self::TYPE_PICKING,
        self::TYPE_PACKAGE,
        self::TYPE_EXIT
    ];

    public $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @Assert\Type("string")
     * @Assert\Choice(choices=Zone::ALLOWED_TYPES, message="Choose a valid type.")
     * @Assert\NotBlank()
     */
    public $type;
}
