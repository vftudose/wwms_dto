<?php

namespace Craft\Dto;

use Craft\Dto\Contract\Arrayable;
use Craft\Utility\StringTransformer;

abstract class DataTransferObject implements Arrayable
{
    public function __construct(array $data = [])
    {
        $this->fromArray($data);
    }

    public function fromArray(array $data): void
    {
        foreach ($data as $key => $value) {
            if ($value === null) {
                continue;
            }

            $property = StringTransformer::toCamelCase($key, '-');
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }

    public function toArray(): array
    {
        $data = [];

        foreach (get_object_vars($this) as $key => $value) {
            $data[StringTransformer::toSnakeCase($key, '-')] = $value;
        }

        return $data;
    }
}