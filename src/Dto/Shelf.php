<?php

namespace Craft\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class Shelf extends DataTransferObject
{
    public $id;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $length;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $width;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $height;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $zoneId;
}
