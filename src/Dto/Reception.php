<?php

namespace Craft\Dto;

use Craft\Utility\StringTransformer;
use Symfony\Component\Validator\Constraints as Assert;

final class Reception extends DataTransferObject
{
    public $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @Assert\Type("array")
     * @Assert\NotBlank()
     */
    public $products;

    public function fromArray(array $data): void
    {
        foreach ($data as $key => $value) {
            if ($value === null) {
                continue;
            }

            $property = StringTransformer::toCamelCase($key, '-');

            if (property_exists($this, $property)) {
                if ($property == 'products' && is_array($value)) {
                    $value = $this->mapArrayToReceptionLines($value);
                }

                $this->{$property} = $value;
            }
        }
    }

    private function mapArrayToReceptionLines(array $items): array
    {
        $data = [];

        foreach ($items as $item) {
            $data[] = new ReceptionLine($item);
        }

        return $data;
    }

    public function toArray(): array
    {
        $data = [];

        foreach (get_object_vars($this) as $key => $value) {
            if ($key == 'products') {
                $value = $this->mapReceptionLinesToArray($value);
            }

            $data[StringTransformer::toSnakeCase($key, '-')] = $value;
        }

        return $data;
    }

    private function mapReceptionLinesToArray(array $items): array
    {
        $data = [];

        /** @var ReceptionLine $item */
        foreach ($items as $item) {
            $data[] = $item->toArray();
        }

        return $data;
    }

    public function getProductsIds(): array
    {
        $ids = [];

        foreach ($this->products as $product) {
            $ids[] = $product->productId;
        }

        return $ids;
    }
}