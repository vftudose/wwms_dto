<?php


namespace Craft\Dto\Order;


use Craft\Dto\DataTransferObject;

class OrderLineTask extends DataTransferObject
{
    public $taskId;

    public $productId;

    public $locationId;

    public $type;

    public $status;
}