<?php

namespace Craft\Dto\Order;

use Craft\Dto\DataTransferObject;
use Craft\Utility\StringTransformer;
use Symfony\Component\Validator\Constraints as Assert;

final class Order extends DataTransferObject
{
    public $id;
    
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @Assert\Type("array")
     * @Assert\NotBlank()
     */
    public $products;

    public function fromArray(array $data): void
    {
        foreach ($data as $key => $value) {
            if ($value === null) {
                continue;
            }

            $property = StringTransformer::toCamelCase($key, '-');

            if (property_exists($this, $property)) {
                if ($property == 'products' && is_array($value)) {
                    $value = $this->mapArrayToOrderLines($value);
                }

                $this->{$property} = $value;
            }
        }
    }

    private function mapArrayToOrderLines(array $items): array
    {
        $data = [];

        foreach ($items as $item) {
            $data[] = new OrderLine($item);
        }

        return $data;
    }

    public function toArray(): array
    {
        $data = [];

        foreach (get_object_vars($this) as $key => $value) {
            if ($key == 'products') {
                $value = $this->mapOrderLinesToArray($value);
            }

            $data[StringTransformer::toSnakeCase($key, '-')] = $value;
        }

        return $data;
    }

    private function mapOrderLinesToArray(array $items): array
    {
        $data = [];

        /** @var OrderLine $item */
        foreach ($items as $item) {
            $data[] = $item->toArray();
        }

        return $data;
    }

    public function getProductsIds(): array
    {
        $ids = [];

        foreach ($this->products as $product) {
            $ids[] = $product->productId;
        }

        return $ids;
    }
}