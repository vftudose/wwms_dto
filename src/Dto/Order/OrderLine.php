<?php

namespace Craft\Dto\Order;

use Craft\Dto\DataTransferObject;
use Craft\Utility\StringTransformer;
use Symfony\Component\Validator\Constraints as Assert;

final class OrderLine extends DataTransferObject
{
    public $id;

    /**
     * @Assert\Type("integer")
     * @Assert\NotBlank()
     */
    public $productId;
    /**
     * @Assert\Type("integer")
     * @Assert\NotBlank()
     */
    public $quantity;

    /**
     * @Assert\Type("integer")
     */
    public $reservedLocationId;

    /**
     * @Assert\Type("array")
     */
    public $tasks;

    public function fromArray(array $data): void
    {
        foreach ($data as $key => $value) {
            if ($value === null) {
                continue;
            }

            $property = StringTransformer::toCamelCase($key, '-');

            if (property_exists($this, $property)) {
                if ($property == 'tasks' && is_array($value)) {
                    $value = $this->fromArrayToOrderLineTasks($value);
                }

                $this->{$property} = $value;
            }
        }
    }

    private function fromArrayToOrderLineTasks(array $items): array
    {
        $data = [];

        foreach ($items as $item) {
            $data[] = new OrderLineTask($item);
        }

        return $data;
    }

    public function toArray(): array
    {
        $data = [];

        foreach (get_object_vars($this) as $key => $value) {
            if ($key == 'tasks' && $value != null) {
                $value = $this->fromOrderLineTasksToArray($value);
            }

            $data[StringTransformer::toSnakeCase($key, '-')] = $value;
        }

        return $data;
    }

    private function fromOrderLineTasksToArray(array $items): array
    {
        $data = [];

        /** @var OrderLine $item */
        foreach ($items as $item) {
            $data[] = $item->toArray();
        }

        return $data;
    }
}
