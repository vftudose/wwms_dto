<?php

namespace Craft\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class Product extends DataTransferObject
{
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public $sku;

    /**
     * @Assert\Type("string")
     */
    public $description;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     */
    public $width;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     */
    public $height;
    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     */
    public $length;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @Assert\GreaterThan(0)
     */
    public $weight;

    /**
     * @Assert\Type("string")
     */
    public $ean;

    /**
     * @Assert\Type("string")
     */
    public $upc;

    /**
     * @Assert\Type("string")
     */
    public $isbn;
}