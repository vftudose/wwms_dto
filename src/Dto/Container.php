<?php

namespace Craft\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class Container extends DataTransferObject
{
    const NORMAL_TYPE = 'normal';

    const ALLOWED_TYPES = [
        self::NORMAL_TYPE
    ];

    public $id;

    /**
     * @Assert\Type("string")
     * @Assert\Choice(choices=Container::ALLOWED_TYPES, message="Choose a valid type.")
     * @Assert\NotBlank()
     */
    public $type;
}
