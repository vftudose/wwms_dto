<?php

namespace Craft\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class Task extends DataTransferObject
{
    const STATUS_UNASSIGNED = 'unassigned';
    const STATUS_ASSIGNED = 'assigned';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_PICKED = 'picked';
    const STATUS_FINISHED = 'finished';

    const TYPE_PICKING = 'picking';
    const TYPE_REPLENISHMENT = 'replenishment';

    const ALLOWED_TYPES = [
        self::TYPE_PICKING,
        self::TYPE_REPLENISHMENT
    ];

    const ALLOWED_STATUSES = [
        self::STATUS_UNASSIGNED,
        self::STATUS_ASSIGNED,
        self::STATUS_IN_PROGRESS,
        self::STATUS_PICKED,
        self::STATUS_FINISHED
    ];

    public $id;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $userId;

    /**
     * @Assert\Type("string")
     */
    public $username;

    /**
     * @Assert\Type("string")
     * @Assert\Choice(choices=Task::ALLOWED_TYPES, message="Choose a valid type.")
     * @Assert\NotBlank()
     */
    public $type;

    /**
     * @Assert\Type("numeric")
     */
    public $orderId;

    /**
     * @Assert\Type("numeric")
     */
    public $receptionId;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $productId;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $locationId;

    /**
     * @Assert\Type("string")
     * @Assert\Choice(choices=Task::ALLOWED_STATUSES, message="Choose a valid type.")
     * @Assert\NotBlank()
     */
    public $status;

    public $transitContainerId;
}