<?php

namespace Craft\Dto\Contract;

interface Arrayable
{
    public function fromArray(array $data): void;
    public function toArray(): array;
}