<?php

namespace Craft\Dto;

use Craft\Dto\DataTransferObject;

class ReceptionLineTask extends DataTransferObject
{
    public $taskId;

    public $productId;

    public $locationId;

    public $type;

    public $status;
}