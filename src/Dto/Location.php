<?php

namespace Craft\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class Location extends DataTransferObject
{
    const TYPE_TRANSIT = 'transit';
    const TYPE_FIXED = 'fixed';

    const ALLOWED_TYPES = [
        self::TYPE_TRANSIT,
        self::TYPE_FIXED
    ];
    
    public $id;

    /**
     * @Assert\Type("string")
     * @Assert\Choice(choices=Location::ALLOWED_TYPES, message="Choose a valid type.")
     * @Assert\NotBlank()
     */
    public $type;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $width;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $height;

    /**
     * @Assert\Type("numeric")
     * @Assert\NotBlank()
     */
    public $length;

    /**
     * @Assert\Type("numeric")
     */
    public $shelfId;
}
